// ------------1-------------

array = [NaN, 0, 88, false, 20, '', undefined, 99, null];

function filterFalse(arr) {
    return arr.filter(function (v) {
        return !!v;
    });
}

console.log(filterFalse(array));

// ------------2-------------
function lengthOfStrings(strings = []) {
    return strings.map((value) => value.length)
}

console.log(lengthOfStrings(['Vitali', 'Chernelivskyi']))


// ------------3-------------
function move(array = [], from = 0, to = 1) {
    const elementFrom = array [from]
    const elementTo = array[to]

    array[from] = elementTo
    array[to] = elementFrom


}

let a = ['Vitali', 'Chernelivskyi']
move(a)
console.log(a) 